const pool = require('../../db')
const queries = require('./queries')

const getStudents = (req, res) => {
    pool.query(queries.getStudents, (error, results) => {
        if (error) {
            console.error('Error executing query:', error);
            res.status(500).send('Internal Server Error');
            return;
        }

        console.log('Query results:', results); // Log the results

        res.status(200).json(results.rows)
    })
}

const getStudentById = (req, res) => {
    //get the id from the url, it comes back as a string, so have to parse it
    const id = parseInt(req.params.id);
    pool.query(queries.getStudentById, [id], (error, results) => {
        if (error) {
            console.error('Error executing query:', error);
            res.status(500).send('Internal Server Error');
            return;
        }

        res.status(200).json(results.rows)
    });
};

const addStudent = (req, res) => {
    // JS destructuring to get the values out of the body object
    const { name, email, age, dob } = req.body;

    // check if email exists
    pool.query(queries.checkEmailExists, [email], (error, results) => {
        if (results.rows.length) {
            res.send("email already exists");
        }
        // add student to db
        pool.query(queries.addStudent, [name, email, age, dob], (error, results) => {
            if (error) {
                console.error('Error executing query:', error);
                res.status(500).send('Internal Server Error');
                return;
            }

            res.status(201).send("student created successfully")
        } )
    })
}

const removeStudent = (req, res) => {
    //get the id from the url, it comes back as a string, so have to parse it
    const id = parseInt(req.params.id);


    pool.query(queries.getStudentById, [id], (error, results) => {
        const noStudentFound = !results.rows.length;
        if (noStudentFound) {
            res.send("Student does not exist in the database")
        }
        pool.query(queries.removeStudent, [id], (error, results) => {
            if (error) {
                console.error('Error executing query:', error);
                res.status(500).send('Internal Server Error');
                return;
            }
            res.status(200).send("Student removed successfully")
        })
    })

}

const updateStudent = (req, res) => {
    const id = parseInt(req.params.id);
    const { name } = req.body;

    pool.query(queries.getStudentById, [id], (error, results) => {
        const noStudentFound = !results.rows.length;
        if (noStudentFound) {
            res.send("Student does not exist in the database")
        }
        pool.query(queries.updateStudent, [name, id], (error, results) => {
            if (error) {
                console.error('Error executing query:', error);
                res.status(500).send('Internal Server Error');
                return;
            }
            res.status(200).send("Student updated successfully")
        })
    })

}

module.exports = {
    getStudents,
    getStudentById,
    addStudent,
    removeStudent,
    updateStudent,
}
